Compiler for the checking of distance bounding attacks as described in the paper.

Use convert.py to compile an input model to a model that can be checked using ProVerif.