from parser import Parser, ParseException, ParseDeclarationException
from tokens import *
import sys

def containsTimer(token):
  if isinstance(token, ProcessStartTimerToken):
    return True
  elif isinstance(token, ProcessIfToken):
    return containsTimer(token.processif) or containsTimer(token.processelse)
  elif isinstance(token, ProcessParallelToken):
    return containsTimer(token.process1) or containsTimer(token.process2)
  elif token == None:
      return False
  elif token.process != None:
    return containsTimer(token.process)
  else:
    return False

def findToken(token, tokenType):
  if token == None:
      return None
  elif isinstance(token, tokenType):
    return token
  elif isinstance(token, ProcessIfToken):
    foundToken = findToken(token.processif, tokenType) 
    if foundToken != None:
      return foundToken
    else:
      return findToken(token.processelse, tokenType)
  elif isinstance(token, ProcessParallelToken):
    foundToken = findToken(token.process1, tokenType)
    if foundToken != None:
      return foundToken
    else:
      return findToken(token.process2, tokenType)
  elif token.process != None:
    return findToken(token.process, tokenType)
  else:
    return None

def findAllTokens(token, tokenType):
  if isinstance(token, tokenType):
    if isinstance(token, ProcessIfToken):
      return [token] + findAllTokens(token.processif, tokenType) + findAllTokens(token.processelse, tokenType)
    elif isinstance(token, ProcessParallelToken):
      return [token] + findAllTokens(token.process1, tokenType) + findAllTokens(token.process2, tokenType)
    elif isinstance(token, ProcessIdentToken):
      return [token]
    elif token.process != None:
      return [token] + findAllTokens(token.process, tokenType)
    else:
      return [token]
  elif isinstance(token, ProcessParallelToken):
    return findAllTokens(token.process1, tokenType) + findAllTokens(token.process2, tokenType)
  elif isinstance(token, ProcessIfToken):
    if token.processelse != None:
      return findAllTokens(token.processif, tokenType) + findAllTokens(token.processelse, tokenType)
    else:
      return findAllTokens(token.processif, tokenType)
  elif token.process != None:
    return findAllTokens(token.process, tokenType)
  else:
    return []

def findTokenParent(token, tokenType):
  if token.process == None:
    return None
  elif isinstance(token.process, tokenType):
    return token
  else:
    return findTokenParent(token.process, tokenType)

def findLastToken(token):
  if isinstance(token, ProcessIfToken):
    if token.processelse != None:
      raise Exception("Don't know how to handle branching")
    elif token.processif != None:
      return findLastToken(token.processif)
    else:
      return token
  elif isinstance(token, ProcessNullToken):
    return token
  elif token.process != None:
    return findLastToken(token.process)
  else:
    return token

def findInTokens(process, parent = None):
  if process == None:
    return []
  elif isinstance(process, ProcessNullToken):
      return []
  elif isinstance(process, ProcessInToken):
    return [(parent, process)] + findInTokens(process.process, process)
  elif isinstance(process, ProcessParallelToken):
     return findInTokens(process.process1, process) + findInTokens(process.process2, process)
  elif isinstance(process, ProcessIfToken):
    return findInTokens(process.processif, process) + findInTokens(process.processelse, process)
  elif process.process != None:
    return findInTokens(process.process, process)
  else:
    return []

def replaceToken(root, original, replacement):
  if root == original:
    return replacement
  elif isinstance(root, ProcessIfToken):
    if root.processif != None:
      root.processif = replaceToken(root.processif, original, replacement)
    if root.processelse != None:
      root.processelse = replaceToken(root.processelse, original, replacement)
    return root
  elif isinstance(root, ProcessParallelToken): 
    root.process1 = replaceToken(root.process1, original, replacement)
    root.process2 = replaceToken(root.process2, original, replacement)
    return root
  elif (not isinstance(root, ProcessIdentToken)) and (not isinstance(root, ProcessNullToken)) and root.process != None:
    root.process = replaceToken(root.process, original, replacement)
    return root
  else:
    return root

def timerToPh(token):
  # Replace the startTimer statement by a phase statement
  startTimerParent = findTokenParent(token, ProcessStartTimerToken)
  startTimerChild = startTimerParent.process.process
  startTimerParent.process = ProcessPhaseToken(1, startTimerChild)

  # Replace the stopTimer statement by a phase statement
  stopTimerParent = findTokenParent(token, ProcessStopTimerToken)
  stopTimerChild = stopTimerParent.process.process
  stopTimerParent.process = ProcessPhaseToken(2, stopTimerChild)

  return token

if len(sys.argv) < 2:
  print "Too few arguments"
  print "Usage: compile.py <input file>"
  exit(-1)

file = open(sys.argv[1])
code = file.read()

parser = Parser()
parser.set_text(code)
try:
  parser.parse()
except ParseException as e:
  print e.msg, e.pos
  print code[e.pos:]
  exit(-1)
except ParseDeclarationException as e: 
  print e.msg, e.pos
  exit(-1)

processIdents = {}

# Name used to contain identity of prover
identityName = "id"

publicChannel = "c"
#attackFlag = "attackFound"
verifiedEvent = "Verified"

verifierProcess = None
proverProcess = None

print "free " + publicChannel + "."

for token in parser.tokens:
  if isinstance(token, LetToken):
    # Non-timed process
    processIdent = str(token.ident)
    processIdents[processIdent] = token

    if containsTimer(token):
      # If a process contains a timer, it must be the verifier process
      if verifierProcess != None:
          raise Exception("More than one timer detected")

      #
      # timerToPh
      #
      verifierProcess = token

      # Process reader
      startTimerParent = findTokenParent(token, ProcessStartTimerToken)
      startTimerChild = startTimerParent.process.process
      startTimerParent.process = ProcessPhaseToken(1, startTimerChild)

      stopTimerParent = findTokenParent(token, ProcessStopTimerToken)
      stopTimerChild = stopTimerParent.process.process
      stopTimerParent.process = ProcessPhaseToken(2, stopTimerChild)

      print token

      # Re,pve the added phase tokens again
      startTimerParent.process = startTimerChild
      stopTimerParent.process = stopTimerChild

      # Remove event(Verified(...))
      for eventToken in findAllTokens(token, ProcessEventToken):
        if eventToken.term.ident.name == verifiedEvent:
          # Remove event token. This will go wrong if event(Verified(...)) is not the last statement of the process.
          replaceToken(token, eventToken, ProcessNullToken())
    else:
      print token
  elif isinstance(token, ProcessToken):
    # The ProcessToken can only occur after all LetToken that define processes have been processed already

    # Split locations and process them depending on whether timer is local or not
    locationListToken = token.process
    locations = locationListToken.locations

    localProcesses = []
    remoteProcesses = []

    localProcessesNames = {}
    remoteProcessesNames = {}

    for location in locations:
        local = False

        # Check if the verifier is one of the processes in this location
        identTokens = findAllTokens(location.process, ProcessIdentToken)

        for identToken in identTokens:
            if verifierProcess != None and identToken.ident.name == verifierProcess.ident.name:
                local = True

        if local:
            localProcesses.append(location.process)
        else:
            remoteProcesses.append(location.process)

        for identToken in identTokens:
            if local:
                localProcessesNames[identToken.ident.name] = True
            else:
                remoteProcessesNames[identToken.ident.name] = True

                # Assume the first process in the remote location is the prover
                if proverProcess == None:
                    proverProcess = processIdents[identToken.ident.name]


    remoteNames = {}
    for remoteProcessName in remoteProcessesNames:
        token = processIdents[remoteProcessName]
        remoteNames[remoteProcessName] = []

        if findToken(token.process, ProcessPhaseToken) != None:
            # The process already contains phase tokens, so we do not touch this process
            continue

        inTokens = findInTokens(token.process, token)
        i = 0

        # Generate new processes
        orig_name = token.ident.name

        # Print original process for every 'in' statement where it is preceded by a 'phase 2' statement
        for (parent, inToken) in inTokens:
            # Check if this is communication on the public channel, if so we restrict it
            if inToken.channel.ident.name == publicChannel:
                parent.process = ProcessPhaseToken(2, inToken)
                token.ident.name = orig_name + '_R' + str(i)
                remoteNames[remoteProcessName].append(token.ident.name)
                i += 1

                print token

                parent.process = inToken
                token.ident.name = orig_name

    localNames = {}
    for localProcessName in localProcessesNames:
        token = processIdents[localProcessName]
        localNames[localProcessName] = []

        inTokens = findInTokens(token.process, token)
        i = 0

        # Generate new processes
        orig_name = token.ident.name

        for (parent, inToken) in inTokens:
            parent.process = ProcessPhaseToken(1, inToken)
            token.ident.name = orig_name + '_L' + str(i)
            localNames[localProcessName].append(token.ident.name)
            i += 1

            print token

            inTokens2 = findInTokens(inToken.process, inToken)

            for (parent2, inToken2) in inTokens2:
              parent2.process = ProcessPhaseToken(2, inToken2)
              token.ident.name = orig_name + '_L' + str(i)
              localNames[localProcessName].append(token.ident.name)
              i += 1

              print token
              parent2.process = inToken2
              token.ident.name = orig_name

            parent.process = inToken
            token.ident.name = orig_name

            parent.process = ProcessPhaseToken(2, inToken)
            token.ident.name = orig_name + '_L' + str(i)
            localNames[localProcessName].append(token.ident.name)
            i += 1

            print token

            parent.process = inToken
            token.ident.name = orig_name

    mainProcess = None

    for localProcess in localProcesses:
      identTokens = findAllTokens(localProcess, ProcessIdentToken)

      for identToken in identTokens:
        proc = identToken
        for name in localNames[identToken.ident.name]:
          proc = ProcessParallelToken(proc, ProcessIdentToken(IdentToken(name)))

        replaceToken(localProcess, identToken, proc)

      if mainProcess == None:
        mainProcess = localProcess
      else:
        mainProcess = ProcessParallelToken(mainProcess, localProcess)

    for remoteProcess in remoteProcesses:
      identTokens = findAllTokens(remoteProcess, ProcessIdentToken)

      for identToken in identTokens:
        proc = identToken

        for name in remoteNames[identToken.ident.name]:
          proc = ProcessParallelToken(proc, ProcessIdentToken(IdentToken(name)))

        replaceToken(remoteProcess, identToken, proc)

      if mainProcess == None:
        mainProcess = remoteProcess
      else:
        mainProcess = ProcessParallelToken(mainProcess, remoteProcess)

    print ProcessToken(mainProcess)
  else:
    print token

if verifierProcess != None:
    print "(* Verifier process: ", verifierProcess.ident.name, "*)"
if proverProcess != None:
    print "(* Prover process: ", proverProcess.ident.name, "*)"
print "(* Public channel: ", publicChannel, "*)"
print "(* Identity name: ", identityName, "*)"