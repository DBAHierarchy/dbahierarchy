class IntervalSet():
    def __init__(self):
        self.list = []
        
    def add(self, start, end, value):
        if len(self.list) == 0:
            self.list.insert(0, Interval(start, end, value))
        else:
            i = 0
            current = self.list[0]
            while (current.start < start) & (i < len(self.list) - 1):
                i = i + 1
                current = self.list[i]
            
            if (current.end < end) & (current.start > end):
                raise Exception('Overlapping intervals')
            
            if (current.end < start):
                i += 1
                
            self.list.insert(i, Interval(start, end, value))
                        
    def get(self, value):
        if len(self.list) == 0:
            return None
        else:
            i = 0
            current = self.list[0]
            while (current.end < value) & (i < (len(self.list) - 1)):
                i = i + 1
                current = self.list[i]
            
            if (current.start <= value) & (value <= current.end):
                return current.value
            
            return None 
                    
    def __str__(self):
        string = '[' + ', '.join(self.list) + ']'
        return string
        
class Interval():
    def __init__(self, start, end, value):
        self.start = start
        self.end = end
        self.value = value
    
    def __str__(self):
        return '{' + str(self.start) + ', ' + str(self.end) + ', ' + str(self.value) + '}'