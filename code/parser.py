from copy import copy
from intervalset import IntervalSet
from tokens import *
import re

REFS_NONE = 1
REFS_ALL = 2
REFS_NOREDUCS = 3

class Parser():
    params = {'traceDisplay': r'(short|long|none)',
                      'verboseRules': r'(yes|true|no|false)',
                      'verboseRedundant': r'(yes|true|no|false)',
                      'verboseCompleted': r'(yes|true|no|false)',
                      'verboseEq': r'(yes|true|no|false)',
                      'verboseTerm': r'(yes|true|no|false)',
                      'maxDepth': r'(\d+|none)',
                      'maxHyp': r'(\d+|none)',
                      'stopTerm': r'(yes|true|no|false)',
                      'selFun': r'(TermMaxsize|Term|NounifsetMaxsize|Nounifset)',
                      'redundancyElim': r'(simple|no|best)',
                      'redundantHypElim': r'(yes|true|no|false|beginOnly)',
                      'reconstructDerivation': r'(yes|true|no|false)',
                      'simplifyDerivation': r'(yes|true|no|false)',
                      'abbreviateDerivation': r'(yes|true|no|false)',
                      'displayDerivation': r'(yes|true|no|false)',
                      'attacker': r'(active|passive)',
                      'keyCompromise': r'(none|approx|strict)',
                      'movenew': r'(yes|true|no|false)',
                      'channels': r'(terms|names)',
                      'predicatesImplementable': r'(check|nocheck)',
                      'verboseExplainClauses': r'(yes|true|no|false)',
                      'explainDerivation': r'(yes|true|no|false)',
                      'reconstructTrace': r'(yes|true|no|false)',
                      'traceBacktracking': r'(yes|true|no|false)',
                      'unifyDerivation': r'(yes|true|no|false)'}
              
    
    def __init__(self):
        self.text = ''
        self.parsed = False

        self.wtext = ''
        self.tokens = []

        self.functions = {}
        self.functions_int = IntervalSet()
        
        self.reduc_vars_int = IntervalSet()

        self.names = {}
        self.names_int = IntervalSet()
        
        self.processes = {}
        self.processes_int = IntervalSet()


    def __str__(self):
        string = ''
        for token in self.tokens:
            string += str(token)
        return string

    def set_text(self, str):
        self.text = str
        self.wtext = str
        self.tokens = []

        self.functions = {}
        self.functions_int = IntervalSet()
        
        self.reduc_vars_int = IntervalSet()

        self.names = {}
        self.names_int = IntervalSet()
        
        self.processes = {}
        self.processes_int = IntervalSet()
        
        self.predicates = ['attacker', 'mess', 'ev', 'evinj']
        
        self.char_total = len(self.text)
        self.parsed = False
        
    def lstrip_text(self):
        self.wtext = self.wtext.lstrip()
        while self.wtext.startswith('(*'):
            pos = self.wtext.find('*)')
            if pos >= 0:
                self.wtext = self.wtext[pos+2:]
                self.lstrip_text()
            else:
                self.wtext = ''
        
    def lstrip_pattern(self, pattern):
        self.wtext = self.wtext[len(pattern):]
        
    def cur_pos(self):
        return self.char_total - len(self.wtext)
    
    def expect(self, pattern):
        pos = self.cur_pos()
        self.lstrip_text()        

        if not self.wtext.startswith(pattern):
            #print self.wtext
            raise ParseException(pattern+" expected", pos)
        self.wtext = self.wtext[len(pattern):]        
        
    def parse_number(self):
        self.lstrip_text()
        match = re.match(r'(\d+)', self.wtext)

        if not match: 
            raise ParseException("Number expected", self.cur_pos())
        
        n = match.group(0)
        self.lstrip_pattern('n')
        
        return n         
        
    def parse(self):
        self.tokens = []

        self.lstrip_text()

        while len(self.wtext) > 0:
            match = re.match(r'(\w+)', self.wtext)
            if not match == None:
                token =  match.group(0)
                
                if token == 'param':
                    self.parse_param()
                elif token == 'private':
                    pos = self.cur_pos()
                    self.lstrip_pattern('private')
                    self.lstrip_text()
                    
                    match = re.match(r'(\w+)', self.wtext)
                    if not match:
                        raise ParseException("String expected", self.cur_pos())
                    str = match.group(0)
                    if str == 'fun':
                        self.parse_fun(True, pos)
                    elif str == 'reduc':
                        self.parse_reduc(True, pos)
                    elif str == 'free':
                        self.parse_free(True, pos)
                    else:
                        raise ParseException("Unexpected string " + token + ". fun, reduc or free expected", self.cur_pos())
                elif token == 'fun':
                    self.parse_fun()                                                                                        
                elif token == 'data':
                    self.parse_data()
                elif token == 'reduc':
                    self.parse_reduc()
                elif token == 'free':
                    self.parse_free()                    
                elif token == 'query':
                    self.parse_seq_query()
                elif token == 'let':
                    self.parse_let()
                elif token == 'equation':
                    self.parse_equation()
                elif token == 'pred':
                    self.parse_pred()
                elif token == 'weaksecret':
                    self.parse_weaksecret()
                elif token == 'noninterf':
                    self.parse_noninterf()
                elif token == 'nounif':
                    self.parse_nounif()
                elif token == 'elimtrue':
                    self.parse_elimtrue()
                elif token == 'not':
                    self.parse_not()
                elif token == 'clauses':
                    self.parse_clauses()
                elif token == 'process':
                    pos = self.cur_pos()
                    self.lstrip_pattern('process')
                    names = copy(self.names)
                    '''
                    process = self.parse_process(names)
                    self.tokens.append(ProcessToken(process, pos, process.end))
                    '''
                    locations = self.parse_location_list(names)
                    self.tokens.append(ProcessToken(locations, pos, locations.end))

                    self.lstrip_text()
                    if len(self.wtext) > 0:
                        raise ParseException("Expected end of file", self.cur_pos())
                else:
                    raise ParseException("Unexpected string " + token, self.cur_pos())
                
                self.lstrip_text()            
            else:
                print self.wtext
                raise ParseException("String expected", self.cur_pos())
        self.parsed = True

    def parse_param(self):
        pos = self.cur_pos()
        # Strip param from text
        self.lstrip_pattern('param')

        # Try to match param syntax
        self.lstrip_text()
        match = re.match(r'(\w+)', self.wtext)
        
        if not match: 
            raise ParseException("String expected", self.cur_pos())
        
        name = match.group(0)
        
        if not name in self.params:
            raise ParseException("Invalid parameter", self.cur_pos())
        self.lstrip_pattern(name)
       
        self.expect('=')

        self.lstrip_text()        
        match = re.match(r'(\w+)', self.wtext)

        if not match: 
            raise ParseException("String expected", self.cur_pos())
        
        value = match.group(0)
        
        if not re.match(self.params[name], value):
            raise ParseException("Invalid value for parameter " + name, self.cur_pos())
        
        self.lstrip_pattern(value)

        self.expect('.')
        
        self.tokens.append(ParamToken(name, value, pos, self.cur_pos()))                
                
    def parse_fun(self, private = False, pos = None):
        if pos == None:
            pos = self.cur_pos()
        
        # Strip fun from text
        self.lstrip_pattern('fun')
                
        # Try to match fun syntax
        ident = self.parse_ident()  

        # Check for double declaration
        if ident.name in self.functions:
            raise ParseDeclarationException("Function " + ident.name + " already declared", ident.start, ident.end)

        self.expect('/')
        
        self.lstrip_text()        
        match = re.match(r'(\d+)', self.wtext)

        if not match: 
            raise ParseException("Number expected", self.cur_pos())
        
        arity = match.group(0)
        self.lstrip_pattern(arity)
                    
        self.expect('.')

        token = FunToken(ident, arity, private, pos, self.cur_pos())

        self.functions[ident.name] = token
        self.functions_int.add(ident.start, ident.end, token)
        #self.functions_refs[ident.name] = []
        self.tokens.append(token)

    def parse_free(self, private = False, pos = None):
        if pos == None:
            pos = self.char_total - len(self.wtext)
        
        idents = []
        
        # Strip free from text
        self.lstrip_pattern('free')
                
        # Try to match free syntax
        ident = self.parse_ident()
        idents.append(ident)

        # Check for double declaration
        if ident.name in self.names:
            raise ParseDeclarationException("Name " + ident.name + " already declared", ident.start, ident.end)
        self.names[ident.name] = ident
        self.names_int.add(ident.start, ident.end, ident)

        while self.wtext.startswith(','):
            self.lstrip_pattern(',')
            ident = self.parse_ident()
            # Check for double declaration
            if ident.name in self.names:
                raise ParseDeclarationException("Name " + ident.name + " already declared", ident.start, ident.end)
            
            idents.append(ident)
            self.names[ident.name] = ident
            self.names_int.add(ident.start, ident.end, ident)
            
        self.expect('.')
        self.tokens.append(FreeToken(idents, private, pos, self.cur_pos()))

    def parse_reduc(self, private = False, pos = None):
        if pos == None:
            pos = self.cur_pos()
        
        reducs = []
        
        # Strip reduc from text
        self.wtext = self.wtext[5:]
        self.lstrip_text()
                
        ident = self.parse_ident()
        name = ident.name
        # Check for double declaration
        if name in self.functions:
            raise ParseDeclarationException("Name " + ident.name + " already declared", ident.start, ident.end)
        
        self.expect('(')
        vars = {}
        terms = self.parse_seq_term(REFS_NOREDUCS, vars, self.reduc_vars_int)
        vars_len = len(vars)
        self.expect(')')
        
        self.expect('=')
          
        result = self.parse_term(REFS_NOREDUCS, vars, self.reduc_vars_int)
        
        # Check whether new variables have been added
        if vars_len < len(vars):
            raise ParseException("Variables used on the right side, should also occur at the left side", self.cur_pos())
        
        token1 = ReducToken(ident, terms, result, private, ident.start, self.cur_pos())
        self.functions[ident.name] = token1
        self.functions_int.add(ident.start, ident.end, token1)
        #self.functions_refs[ident.name] = []
        reducs.append(token1)
        
        while self.wtext.startswith(';'):
            self.lstrip_pattern(';')
            # Try to match reduc syntax
            ident = self.parse_ident()
    
            # Check for double declaration
            if not ident.name == name:
                raise ParseException("In reducs all reductions should start with the same function " + name, self.cur_pos())
            
            self.expect('(')    
            vars = {}            
            terms = self.parse_seq_term(REFS_NOREDUCS, vars, self.reduc_vars_int)
            vars_len = len(vars)            
            self.expect(')')
           
            self.expect('=')
              
            result = self.parse_term(REFS_NOREDUCS, vars, self.reduc_vars_int)
            
            if vars_len < len(vars):
                raise ParseException("Variables used on the right side, should also occur at the left side")            
            
            token2 = ReducToken(ident, terms, result, private, ident.pos, self.cur_pos())
            #self.functions[ident.name] = terms.size()
            #self.functions_tokens[ident.name] = token
            token1.ident.refs.append(ident)
            token2.ident.declaration = token1.ident
            self.functions_int.add(ident.start, ident.end, token1)            
            #self.functions_refs[ident.name].append(token.ident)
            reducs.append(token2)            
            
        self.expect('.')

        self.tokens.append(ReducSeqToken(reducs, private, pos, self.cur_pos()))
        
    def parse_seq_term(self, refs = REFS_NONE, refs_dict = None, refs_int = None):
        # Zero or more terms separated by commas
        self.lstrip_text()
        
        terms = []
        pos = self.cur_pos()
        
        terms.append(self.parse_term(refs, refs_dict, refs_int))
        
        self.lstrip_text()
        while self.wtext.startswith(','):
            self.lstrip_pattern(',')
            terms.append(self.parse_term(refs, refs_dict, refs_int))
            self.lstrip_text()

        return TermSeqToken(terms, pos, self.cur_pos())
    
    def parse_seq_ident(self):
        self.lstrip_text()        
        pos = self.cur_pos()
        
        idents = []
        idents.append(self.parse_ident())
        self.lstrip_text()
        
        while self.wtext.startswith(','):
            self.lstrip_pattern(',')
            idents.append(self.parse_ident())
            
        return IdentSeqToken(idents, pos, self.cur_pos())
        
    def parse_ident(self):
        self.lstrip_text()        
        pos = self.cur_pos()
        
        match = re.match(r'(\w[\w\']*)', self.wtext)
        if not match: 
            raise ParseException("Identifier expected", self.cur_pos())
        
        name = match.group(0)
        self.lstrip_pattern(name)

        token = IdentToken(name, pos, self.cur_pos())

        return token
        
    def parse_term(self, refs = REFS_NONE, refs_dict = None, refs_int = None):
        #term ::= ident (seq term )
        #    | (seq term )
        #    | ident
        #    | choice[ term , term ]

        self.lstrip_text()        
        pos = self.cur_pos()
                    
        if self.wtext.startswith('('):
            self.lstrip_pattern('(')
            self.lstrip_text()
            if self.wtext.startswith(')'):
                self.lstrip_pattern(')')
                return TermSeqToken([], pos, self.cur_pos())
            else:
                term = self.parse_seq_term(refs, refs_dict, refs_int)
                self.expect(')')
                return term
        elif self.wtext.startswith('choice['):
            self.lstrip_pattern('choice[')
            term1 = self.parse_term(refs, refs_dict, refs_int)
            self.expect(',')
            term2 = self.parse_term(refs, refs_dict, refs_int)
            self.expect(']')
            
            return TermChoiceToken(term1, term2, pos, self.cur_pos())
        
        ident = self.parse_ident()

        self.lstrip_text()
        if not self.wtext.startswith('('):
            if refs in [REFS_ALL, REFS_NOREDUCS]:
                if ident.name in self.functions:
                    if not self.functions[ident.name].arity == 0:
                        raise ParseException("Wrong number of arguments when using  "+ident.name + " 0 instead of " + str(self.functions[ident.name].arity), self.cur_pos())

                    self.functions[ident.name].ident.refs.append(ident)
                    self.functions_int.add(ident.start, ident.end, self.functions[ident.name])
                    
                elif not refs_dict == None: 
                    if not ident.name in refs_dict:
                        refs_dict[ident.name] = ident
                        refs_int.add(ident.start, ident.end, ident)
                    else:
                        refs_dict[ident.name].refs.append(ident)
                        refs_int.add(ident.start, ident.end, refs_dict[ident.name])
                                        
            return TermIdentToken(ident, pos, self.cur_pos())
        else:
            #if not ident.name in self.functions:
                #raise ParseException("Name "+ident.name+" not declared")
            self.lstrip_pattern('(')
            pos2 = self.cur_pos() - 1
            self.lstrip_text()
            
            if self.wtext.startswith(')'):
                args = TermSeqToken([], pos2, self.cur_pos())
            else:            
                args = self.parse_seq_term(refs, refs_dict, refs_int)
            self.expect(')')
            
            if refs in [REFS_ALL, REFS_NOREDUCS]:
                if not ident.name in self.functions:
                    raise ParseDeclarationException(ident.name + " not declared", ident.start, ident.end)
                self.functions[ident.name].ident.refs.append(ident)
                
                #self.functions_refs[ident.name].append(ident)
                if refs == REFS_NOREDUCS:
                    if isinstance(self.functions[ident.name], ReducToken):
                        raise ParseException('Reduction ' + ident.name + ' cannot be used inside a reduction', self.cur_pos())
                
                if not (args.size() == self.functions[ident.name].arity):
                    raise ParseException("Wrong number of arguments when using  "+ident.name + " "+ str(args.size())+" instead of " + str(self.functions[ident.name].arity), self.cur_pos())
            token = TermApplyToken(ident, args, pos, self.cur_pos())
            
            if refs in [REFS_ALL, REFS_NOREDUCS]:
                token.declaration = self.functions[ident.name] 
                self.functions_int.add(ident.start, ident.end, self.functions[ident.name])
                
            return token
        
    def parse_data(self):
        pos = self.cur_pos()
        
        # Strip data from text
        self.lstrip_pattern('data')
                
        ident = self.parse_ident()

        # Check for double declaration
        if ident.name in self.functions:
            raise ParseDeclarationException("Name " + ident.name + " already declared", ident.start, ident.end)
        
        self.expect('/')        

        match = re.match(r'(\d+)', self.wtext)
        if not match: 
            raise ParseException("Number expected", self.cur_pos())
        arity = match.group(0)
        self.lstrip_pattern(arity)
            
        self.expect('.')

        token = DataToken(ident, arity, pos, self.cur_pos())
        self.functions[ident.name] = token
        self.functions_int.add(ident.start, ident.end, token)
        #self.functions_refs[ident.name] = []
        self.tokens.append(token)

    def parse_seq_query(self):
        pos = self.cur_pos()
        self.lstrip_pattern('query')
        
        queries = []
        
        queries.append(self.parse_query())
        
        self.lstrip_text()
        
        while self.wtext.startswith(';'):
            queries.append(self.parse_query())
            self.lstrip_text()
            
        self.expect('.')
        
        self.tokens.append(QuerySeqToken(queries, pos, self.cur_pos()))
    
    def parse_query(self):
        self.lstrip_text()
        pos = self.cur_pos()
        
        if self.wtext.startswith('putbegin'):
            self.lstrip_pattern('putbegin')
            self.lstrip_text()
            #TODO has to be separated by either whitespace or comment
            if self.wtext.startswith('evinj'):
                self.expect(':')
                idents = self.parse_seq_ident()
                return QueryPutbeginEvInjToken(idents, pos, self.cur_pos())
            elif self.wtext.startswith('ev'):
                self.expect(':')
                idents = self.parse_seq_ident()
                return QueryPutbeginEvToken(idents, pos, self.cur_pos())
            else:
                raise ParseException("evinj or ev expected", self.cur_pos())
        elif self.wtext.startswith('let'):
            self.lstrip_pattern('let')
            ident = self.parse_ident()
            self.expect('=')
            term = self.parse_gterm()
            return QueryLetToken(ident, term, pos, self.cur_pos())
        else:
            fact = self.parse_gfact()
            end = self.cur_pos()
            self.lstrip_text()
            
            if self.wtext.startswith('==>'):
                self.lstrip_pattern('==>')
                hyp = self.parse_hyp()
                return QueryRealToken(fact, hyp, pos, self.cur_pos())
            else:
                return QueryGFactToken(fact, pos, end)

    def parse_gfact(self, allow_single_ident = False):
        # allow_single_ident is used to support backwards compatibility for the not construct:
        # not A. = not attacker:A.
        self.lstrip_text()
        pos = self.cur_pos()
        
        term1 = self.parse_gterm()
        self.lstrip_text()
        
        if self.wtext.startswith(':'):
            if not isinstance(term1, GTermIdentToken):
                raise ParseException("Unexpected :", self.cur_pos())
            self.lstrip_pattern(':')
            terms = self.parse_seq_gterm()
            #TODO fix, does not work without brackets
            end = self.cur_pos()
            self.lstrip_text()
            
            if self.wtext.startswith('phase'):
                self.lstrip_text() 
                match = re.match(r'(\d+)', self.wtext)
    
                if not match: 
                    raise ParseException("Number expected", self.cur_pos())
            
                n = match.group(0)
                return GFactIdentPhaseToken(term1.ident, terms, n, pos, self.cur_pos())
            else:
                return GFactIdentToken(term1.ident, terms, pos, end)
        elif self.wtext.startswith('<>'):
            self.lstrip_pattern('<>')
            term2 = self.parse_gterm()
            return GFactUnequalToken(term1, term2, pos, self.cur_pos())
        elif self.wtext.startswith('='):
            self.lstrip_pattern('=')
            term2 = self.parse_gterm()
            return GFactEqualToken(term1, term2, pos, self.cur_pos())
        else:
            if allow_single_ident:
                return GFactIdentToken(term1.ident, None, pos, term1.end)
            else:
                raise ParseException('Unexpected character', self.cur_pos())
    
    def parse_hyp(self):
        self.lstrip_text()
        pos = self.cur_pos()
        
        if self.wtext.startswith('('):
            self.lstrip_pattern('(')
            sub_hyp = self.parse_hyp();
            self.expect(')')
            hyp1 = HypBracketToken(sub_hyp, pos, self.cur_pos())
        else:
            fact = self.parse_gfact()
            end = self.cur_pos()
            self.lstrip_text() 
            if self.wtext.startswith('==>'):
                self.lstrip_pattern('==>')
                hyp = self.parse_hyp()
                query = QueryRealToken(fact, hyp, pos, self.cur_pos())
                hyp1 = HypRealQueryToken(query, pos, self.cur_pos())
            else:
                hyp1 = HypGFactToken(fact, pos, end)
        
        self.lstrip_text() 
        if self.wtext.startswith('|'):
            hyp2 = self.parse_hyp()
            return HypOrToken(hyp1, hyp2, pos, self.cur_pos())
        elif self.wtext.startswith('&'):
            hyp2 = self.parse_hyp()
            return HypAndToken(hyp1, hyp2, pos, self.cur_pos())
        else:
            return hyp1            
    
    def parse_gterm(self):
        self.lstrip_text()
        pos = self.cur_pos()

        if self.wtext.startswith('('):
            self.lstrip_pattern('(')
            term = self.parse_seq_gterm()
            self.expect(')')
            return term

        ident = self.parse_ident()
        #TODO check whether ident is already declared?
        self.lstrip_text()
        #print self.wtext
        if self.wtext.startswith('('):
            self.lstrip_pattern('(')
            self.lstrip_text()
            if self.wtext.startswith(')'):
                args = None
            else:
                args = self.parse_seq_gterm()
            self.expect(')')
            return GTermApplyToken(ident, args, pos, self.cur_pos())
        elif self.wtext.startswith('['):
            args = self.parse_seq_gbinding()
            return GTermBindToken(ident, args, pos, self.cur_pos())
        else:
            return GTermIdentToken(ident, pos, self.cur_pos())       
        
    def parse_seq_gterm(self):
        # Zero or more terms separated by commas
        self.lstrip_text()
        
        terms = []
        pos = self.cur_pos()
        
        terms.append(self.parse_gterm())
        
        self.lstrip_text()
        while self.wtext.startswith(','):
            self.lstrip_pattern(',')
            terms.append(self.parse_gterm())
            self.lstrip_text()

        return GTermSeqToken(terms, pos, self.cur_pos())
    
    def parse_seq_gbinding(self):
        self.lstrip_text()
        
        terms = []
        pos = self.cur_pos()
        
        self.expect('[')
        if self.wtext.startswith(')'):
            return GBindingSeqToken(terms, pos, self.cur_pos())
        
        terms.append(self.parse_binding())
        
        self.lstrip_text()
        while self.wtext.startswith(','):
            self.lstrip_pattern(',')
            terms.append(self.parse_gbinding())
            self.lstrip_text()
            
        self.expect(']')
        
        return GBindingSeqToken(terms, pos, self.cur_pos())
    
    def parse_gbinding(self):
        self.lstrip_text()
        pos = self.cur_pos()
        
        if self.wtext.startswith('!'):
            self.lstrip_pattern('!')
            self.lstrip_text()        
            match = re.match(r'(\d+)', self.wtext)

            if not match: 
                raise ParseException("Number expected", self.cur_pos())
        
            n = match.group(0)
            
            self.expect('=')
            term = self.parse_gterm()

            return GBindingNToken(n, term, pos, self.cur_pos())
        else:
            ident = self.parse_ident()
            self.expect('=')
            term = self.parse_gterm()            
            return GBindingIdentToken(ident, term, pos, self.cur_pos())               

    def parse_let(self):
        self.lstrip_text()
        pos = self.cur_pos()
        
        self.lstrip_pattern('let')
        ident = self.parse_ident()
        self.expect('=')
        
        names = copy(self.names)        
        process = self.parse_process(names)
        self.expect('.')
        
        token = LetToken(ident, process, pos, self.cur_pos())
        self.processes[ident.name] = token
        self.processes_int.add(ident.start, ident.end, token)
        self.tokens.append(token) 
    
    def parse_process(self, names):
        names_orig = copy(names)
        self.lstrip_text()
        pos = self.cur_pos()

        if self.wtext.startswith('0'):
            self.lstrip_pattern('0')
            process = ProcessNullToken(pos, self.cur_pos())
        elif self.wtext.startswith('('):
            self.lstrip_pattern('(')
            sub_process = self.parse_process(names)
            self.expect(')')
            process = ProcessBracketsToken(sub_process, pos, self.cur_pos())
        elif self.wtext.startswith('!'):
            self.lstrip_pattern('!')
            sub_process = self.parse_process(names)
            process = ProcessBankToken(sub_process, pos, sub_process.end)
        elif self.wtext.startswith('new'):
            self.lstrip_pattern('new')
            ident = self.parse_ident()
            end = self.cur_pos()
            self.lstrip_text()

            if ident.name in names:
                #TODO Warning name rebound
                pass
            
            names[ident.name] = ident
            self.names_int.add(ident.start, ident.end, ident)


            sub_process = None

            if self.wtext.startswith(';'):
                self.lstrip_pattern(';')
                sub_process = self.parse_process(names)
                end = sub_process.end

            process = ProcessNewToken(ident, sub_process, pos, end)
        elif self.wtext.startswith('if'):
            names_copy = copy(names)
            self.lstrip_pattern('if')
            fact = self.parse_fact(REFS_ALL, names, self.names_int)
            self.expect('then')
            if_process = self.parse_process(names)
            end = if_process.end
            self.lstrip_text()
            else_process = None
            
            if self.wtext.startswith('else'):
                self.lstrip_pattern('else')
                else_process = self.parse_process(names_copy)
                end = else_process.end
                
            process = ProcessIfToken(fact, if_process, else_process, pos, end)
        elif self.wtext.startswith('in'):#TODO This gives problems when using processes starting with 'in'. Should check if it is followed by whitespace or '('
            self.lstrip_pattern('in')
            self.expect('(')
            channel = self.parse_term(REFS_ALL, names, self.names_int)
            self.expect(',')
            pattern = self.parse_pattern(None, names, self.names_int)
            self.expect(')')
            end = self.cur_pos()
            self.lstrip_text()
            sub_process = None
            
            if self.wtext.startswith(';'):
                self.lstrip_pattern(';')
                sub_process = self.parse_process(names)
                end = sub_process.end
                
            process = ProcessInToken(channel, pattern, sub_process, pos, end)     
        elif self.wtext.startswith('out'):#TODO This gives problems when using processes starting with 'out'. Should check if it is followed by whitespace or '('
            self.lstrip_pattern('out')
            self.expect('(')
            channel = self.parse_term(REFS_ALL, names, self.names_int)
            self.expect(',')
            term = self.parse_term(REFS_ALL, names, self.names_int)
            self.expect(')')
            end = self.cur_pos()
            self.lstrip_text()
            sub_process = None
            
            if self.wtext.startswith(';'):
                self.lstrip_pattern(';')
                sub_process = self.parse_process(names)
                end = sub_process.end
                
            process = ProcessOutToken(channel, term, sub_process, pos, end)     
        elif self.wtext.startswith('let'):
            names_copy = copy(names)
            
            self.lstrip_pattern('let')
            self.lstrip_text()
            ident = None
            let = False
            
            if not (self.wtext.startswith('(') | self.wtext.startswith('=')):
                ident = self.parse_ident()

                if ident.name in names:
                    names[ident.name].refs.append(ident)
                    self.names_int.add(ident.start, ident.end, names[ident.name])
                else:
                    names[ident.name] = ident
                    self.names_int.add(ident.start, ident.end, ident)
                                        
                self.lstrip_text()   
                if not (self.wtext.startswith('(') | self.wtext.startswith('=')):
                    idents = [ident]
                                            
                    while self.wtext.startswith(','):
                        ident2 = self.parse_ident()
                        if ident2.name in names:
                            names[ident2.name].refs.append(ident2)
                            self.names_int.add(ident2.start, ident2.end, names[ident2.name])
                        else:
                            names[ident2.name] = ident2
                            self.names_int.add(ident2.start, ident2.end, ident2)
                                                
                        idents.append(ident2)
                        self.lstrip_text()
                        
                    identseq = IdentSeqToken(idents, ident.start, idents[len(idents)-1].end)
                    self.expect('suchthat')
                    fact = self.parse_fact(names, self.names_int)
                    self.expect('in')
                    sub_process = self.parse_process(names)
                    end = sub_process.end
                    self.lstrip_text()
                    else_process = None
                    
                    if self.wtext.startswith('else'):
                        self.lstrip_pattern('else')
                        else_process = self.parse_process(names_copy)
                        end = else_process.end
                        
                    process = ProcessLetSuchthatToken(identseq, sub_process, else_process, pos, end)                    
                else:
                    let = True
            else:
                let = True
                
            if let:
                pattern = self.parse_pattern(ident, names, self.names_int)
                self.expect('=')
                term = self.parse_term(REFS_ALL, names, self.names_int)
                self.expect('in')
                sub_process = self.parse_process(names)
                end = sub_process.end
                self.lstrip_text()
                else_process = None
                
                if self.wtext.startswith('else'):
                    self.lstrip_pattern('else')
                    else_process = self.parse_process(names_copy)
                    end = else_process.end
                    
                process = ProcessLetToken(pattern, term, sub_process, else_process, pos, end)                
        elif self.wtext.startswith('event'):
            self.lstrip_pattern('event')
            term = self.parse_term()
            
            end = self.cur_pos()
            self.lstrip_text()
            sub_process = None
            
            if self.wtext.startswith(';'):
                self.lstrip_pattern(';')
                sub_process = self.parse_process(names)
                end = sub_process.end
                
            process = ProcessEventToken(term, sub_process, pos, end)
        elif self.wtext.startswith('phase'):
            self.lstrip_pattern('phase')
            self.lstrip_text()     

            match = re.match(r'(\d+)', self.wtext)

            if not match: 
                raise ParseException("Number expected", self.cur_pos())
        
            n = match.group(0)            
            self.lstrip_pattern(n)

            end = self.cur_pos()
            self.lstrip_text()
            sub_process = None
            
            if self.wtext.startswith(';'):
                self.lstrip_pattern(';')
                sub_process = self.parse_process(names)
                end = sub_process.end
                
            process = ProcessPhaseToken(n, sub_process, pos, end)
        elif self.wtext.startswith('startTimer'):
            self.lstrip_pattern('startTimer')
            self.lstrip_text()     

            end = self.cur_pos()
            self.lstrip_text()
            sub_process = None
            
            if self.wtext.startswith(';'):
                self.lstrip_pattern(';')
                sub_process = self.parse_process(names)
                end = sub_process.end
                
            process = ProcessStartTimerToken(sub_process, pos, end)
        elif self.wtext.startswith('stopTimer'):
            self.lstrip_pattern('stopTimer')
            self.lstrip_text()     

            end = self.cur_pos()
            self.lstrip_text()
            sub_process = None
            
            if self.wtext.startswith(';'):
                self.lstrip_pattern(';')
                sub_process = self.parse_process(names)
                end = sub_process.end
                
            process = ProcessStopTimerToken(sub_process, pos, end)
        else:
            ident = self.parse_ident()
            process = ProcessIdentToken(ident, pos, self.cur_pos())
            
            if ident.name in self.processes:
                self.processes[ident.name].ident.refs.append(ident)
                self.processes_int.add(ident.start, ident.end, self.processes[ident.name])
            else:
                # TODO
                pass
            
        self.lstrip_text()

        if self.wtext.startswith('|'):
            self.lstrip_pattern('|')
            #TODO not correct, variables should be able to be declared in parallel
            process2 = self.parse_process(names_orig)
            return ProcessParallelToken(process, process2, pos, process2.end)
        else:
            return process
        
    def parse_pattern(self, ident = None, refs_dict = None, refs_int = None):
        #TODO implement references 
        self.lstrip_text()
        pos = self.cur_pos()
        
        if self.wtext.startswith('('):
            self.lstrip_pattern('(')
            seq = self.parse_seq_pattern(refs_dict, refs_int)
            self.expect(')')
            
            if ident == None:
                return PatternBracketsToken(seq, pos, self.cur_pos())
            else:
                return PatternApplyToken(ident, seq, ident.start, self.cur_pos())
        elif not ident == None:
            return PatternIdentToken(ident, ident.start, ident.end)
        elif self.wtext.startswith('='):
            self.lstrip_pattern('=')
            term = self.parse_term(REFS_ALL, refs_dict, refs_int)
            return PatternEqualToken(term, pos, self.cur_pos())
        else:
            ident = self.parse_ident()
            
            if not refs_dict == None:
                refs_dict[ident.name] = ident
                refs_int.add(ident.start, ident.end, ident)
            
            if self.wtext.startswith('('):
                self.lstrip_pattern('(')
                seq = self.parse_seq_pattern(None, refs_dict, refs_int)
                self.expect(')')            
                return PatternApplyToken(ident, seq, pos, self.cur_pos())
            else:
                return PatternIdentToken(ident, pos, ident.end)
    
    def parse_seq_pattern(self, refs_dict = None, refs_int = None):
        self.lstrip_text()
        pos = self.cur_pos()
        
        patterns = []
        
        patterns.append(self.parse_pattern(None, refs_dict, refs_int))
        
        self.lstrip_text()
        while self.wtext.startswith(','):
            self.lstrip_pattern(',')
            patterns.append(self.parse_pattern(None, refs_dict, refs_int))
            self.lstrip_text()
            
        return PatternSeqToken(patterns, pos, patterns[len(patterns)-1].end)
            
                
    def parse_fact(self, refs = REFS_NONE, refs_dict = None, refs_int = None):
        self.lstrip_text()
        pos = self.cur_pos()
        
        term1 = self.parse_term(refs, refs_dict, refs_int)
        self.lstrip_text()
        
        if self.wtext.startswith(':'):
            if not isinstance(term1, TermIdentToken):
                raise ParseException("Unexpected :", self.cur_pos())
            self.lstrip_pattern(':')
            terms = self.parse_seq_term()
            #TODO term1 should be converted to IdentToken
            return FactIdentToken(term1, terms, pos, self.cur_pos())
        elif self.wtext.startswith('<>'):
            self.lstrip_pattern('<>')
            term2 = self.parse_term(refs, refs_dict, refs_int)
            return FactUnequalToken(term1, term2, pos, self.cur_pos())
        elif self.wtext.startswith('='):
            self.lstrip_pattern('=')
            term2 = self.parse_term(refs, refs_dict, refs_int)
            return FactEqualToken(term1, term2, pos, self.cur_pos())
        else:
            raise ParseException(":, <> or = expected", self.cur_pos())
        
    def parse_equation(self):
        pos = self.cur_pos()
        self.lstrip_pattern('equation')
        
        vars = {}
        
        term1 = self.parse_term(REFS_ALL, vars, self.names_int)
        self.expect('=')
        term2 = self.parse_term(REFS_ALL, vars, self.names_int)
        self.expect('.')
        self.tokens.append(EquationToken(term1, term2, pos, self.cur_pos()))
                  
    def parse_pred(self):
        pos = self.cur_pos()
        self.lstrip_pattern('pred')
        
        ident = self.parse_ident()
        
        if ident.name in self.predicates:
            raise ParseException('Predicate ' + ident.name + ' already declared', self.cur_pos())
        
        self.predicates.append(ident.name)
        
        self.expect('/')
        
        n = self.parse_number()
        
        self.lstrip_text()
        
        # Properties can be empty
        if self.wtext.startswith('.'):
            self.lstrip_pattern('.')
            self.tokens.append(PredToken(ident, n, [], pos, self.cur_pos()))
        else:
            idents = self.parse_seq_ident()
            
            for id in idents.idents:
                if not id.name in ['block', 'elimVar', 'elimVarStrict', 'decompData', 'memberOptim']:
                    raise ParseException('Only block, elimVar, elimVarStrict, decompData and memberOptim are allowed as properties of predicates', self.cur_pos()) 
            
            self.expect('.')        
            self.tokens.append(PredToken(ident, n, idents, pos, self.cur_pos())) 

    def parse_weaksecret(self):
        pos = self.cur_pos()
        self.lstrip_pattern('weaksecret')
        ident = self.parse_ident()
        self.expect('.')
        self.tokens.append(WeaksecretToken(ident, pos, self.cur_pos()))
        
    def parse_noninterf(self):
        pos = self.cur_pos()
        self.lstrip_pattern('noninterf')
        interfspecs = self.parse_seq_interfspec()
        self.expect('.')
        self.tokens.append(NoninterfToken(interfspecs, pos, self.cur_pos()))
        
    def parse_seq_interfspec(self):
        self.lstrip_text()
        pos = self.cur_pos()
        
        interfspecs = []
        interfspecs.append(self.parse_interfspec())
        end = self.cur_pos()
        self.lstrip_text()
        
        while self.wtext.startswith(','):
            self.lstrip_pattern(',')
            interfspecs.append(self.parse_interfspec())
            end = self.cur_pos()
            
        return InterfspecSeqToken(interfspecs, pos, end)
        

    def parse_interfspec(self):
        self.lstrip_text()        
        pos = self.cur_pos()
      
        ident = self.parse_ident()
        end = self.cur_pos()
        self.lstrip_text()
        terms = None
        
        if self.wtext.startswith('among'):
            self.lstrip_pattern('among')
            self.expect('(')
            terms = self.parse_seq_term()
            self.expect(')')
            end = self.cur_pos()
            
        return InterfspecToken(ident, terms, pos, end)        

    def parse_nounif(self):
        pos = self.cur_pos()
        self.lstrip_pattern('nounif')
        
        self.lstrip_text()
       
        gfactformat = self.parse_gfactformat()
        
        end = self.cur_pos()
        n = None
        
        self.lstrip_text()
        
        if self.wtext.startswith('/'):
            self.lstrip_pattern('/')
            n = self.parse_number()
            end = self.cur_pos()
        
        self.lstrip_text()
        
        b = []
        while self.wtext.startswith(';'):
            self.lstrip_pattern(';')
            ident = self.parse_ident()
            gtermformat = self.parse_gtermformat()
            b.append(BindToken(ident, gtermformat, ident.start, self.cur_pos()))
            self.lstrip_text()
            
        self.expect('.')
        
        self.tokens.append(NounifToken(gfactformat, n, b, pos, end))
        
    def parse_gfactformat(self):
        self.lstrip_text()
        pos = self.cur_pos()
        
        ident = self.parse_ident()
        
        self.expect(':')
        
        gtermformats = self.parse_seq_gtermformat()
        
        end = self.cur_pos()
        
        n = None
        
        self.lstrip_text()
        if self.wtext.startswith('phase'):
            self.lstrip_pattern('phase')
            n = self.parse_number()
            end = self.cur_pos()
            
        return GFactformatToken(ident, gtermformats, n, pos, end)
        
    def parse_gtermformat(self):
        self.lstrip_text()
        pos = self.cur_pos()

        if self.wtext.startswith('('):
            self.lstrip_pattern('(')
            term = self.parse_seq_gtermformat()
            self.expect(')')
            return term
        elif self.wtext.startswith('*'):
            ident = self.parse_ident()
            return GTermformatStarToken(ident, pos, self.cur_pos())
        
        ident = self.parse_ident()
        #TODO check whether ident is already declared?
        self.lstrip_text()
        #print self.wtext
        if self.wtext.startswith('('):
            self.lstrip_pattern('(')
            self.lstrip_text()
            if self.wtext.startswith(')'):
                args = None
            else:
                args = self.parse_seq_gterm()
            self.expect(')')
            return GTermformatApplyToken(ident, args, pos, self.cur_pos())
        elif self.wtext.startswith('['):
            args = self.parse_seq_gbinding()
            return GTermformatBindToken(ident, args, pos, self.cur_pos())
        else:
            return GTermformatIdentToken(ident, pos, self.cur_pos())       
        
    def parse_seq_gtermformat(self):
        # Zero or more terms separated by commas
        self.lstrip_text()
        
        terms = []
        pos = self.cur_pos()
        
        terms.append(self.parse_gterm())
        
        self.lstrip_text()
        while self.wtext.startswith(','):
            self.lstrip_pattern(',')
            terms.append(self.parse_gterm())
            self.lstrip_text()

        return GTermformatSeqToken(terms, pos, self.cur_pos())        
        
    def parse_factformat(self):
        self.lstrip_text()
        pos = self.cur_pos()
        
        ident = self.parse_ident()
        
        self.expect(':')
        
        termformats = self.parse_seq_termformat()
        
        return FactformatToken(ident, termformats, pos, termformats.end)
    
    def parse_seq_termformat(self):
        self.lstrip_text()
        pos = self.cur_pos()
        
        termformats = [self.parse_termformat()]
        
        self.lstrip_text()
        
        while self.wtext.startswith(','):
            termformats.append(self.parse_termformat())
            self.lstrip_text()
            
        return TermformatSeqToken(termformats, pos, termformats[len(termformats)-1].end) 
        
    def parse_termformat(self):
        self.lstrip_text()
        pos = self.cur_pos()
        
        if self.wtext.startswith('('):
            self.lstrip_pattern('(')
            termformats = self.parse_seq_termformat()
            self.expect(')')
            return termformats
        elif self.wtext.startswith('*'):
            self.lstrip_pattern('*')
            ident = self.parse_ident()
            return TermformatStarToken(ident, pos, self.cur_pos())
        else:
            ident = self.parse_ident()
            
            self.lstrip_text()
            
            if self.wtext.startswith('('):
                self.lstrip_pattern('(')
                termformats = self.parse_seq_termformat()
                self.expect(')')
                return TermformatApplyToken(ident, termformats, pos, self.cur_pos())
            else:
                return TermformatIdentToken(ident, pos, ident.end)
            
    def parse_elimtrue(self):
        pos = self.cur_pos()
        self.lstrip_pattern('elimtrue')
        
        self.lstrip_text()
        
        factformat = self.parse_factformat()
        
        self.expect('.')
        
        self.tokens.append(ElimtrueToken(factformat, pos, self.cur_pos()))
        
    def parse_not(self):
        pos = self.cur_pos()
        self.lstrip_pattern('not')
        
        self.lstrip_text()
        
        gfact = self.parse_gfact(True)
        
        end = self.cur_pos()
        
        self.lstrip_text()
        
        b = []
        while self.wtext.startswith(';'):
            self.lstrip_pattern(';')
            ident = self.parse_ident()
            gtermformat = self.parse_gtermformat()
            b.append(BindToken(ident, gtermformat, ident.start, self.cur_pos()))
            end = self.cur_pos()
            self.lstrip_text()
            
        self.expect('.')
        
        self.tokens.append(NotToken(gfact, b, pos, end))
        
    def parse_clauses(self):
        pos = self.cur_pos()
        self.lstrip_pattern('clauses')
        self.lstrip_text()
        
        rules = [self.parse_rule()]
        
        self.lstrip_text()
        while self.wtext.startswith(';'):
            self.lstrip_pattern(';')
            rules.append(self.parse_rule())
            self.lstrip_text()
            
        self.expect('.')
        
        self.tokens.append(ClausesToken(rules, pos, rules[len(rules)-1].end))
        
    def parse_rule(self):
        self.lstrip_text()
        pos = self.cur_pos()
        
        facts = [self.parse_fact()]
        self.lstrip_text()
        
        while self.wtext.startswith('&'):
            facts.append(self.parse_fact())
            self.lstrip_text()
        
        if self.wtext.startswith('->'): 
            self.lstrip_pattern('->')
            fact = self.parse_fact()
            return RuleImpToken(facts, fact, pos, self.cur_pos())
        elif self.wtext.startswith('<->'):
            self.lstrip_pattern('<->')
            fact = self.parse_fact()
            return RuleBiImpToken(facts, fact, pos, self.cur_pos())
        elif self.wtext.startswith('<=>'):
            self.lstrip_pattern('<=>')
            fact = self.parse_fact()
            return RuleDoubleBiImpToken(facts, fact, pos, self.cur_pos())
        else:
            return RuleFactToken(fact, pos, self.cur_pos())

    def parse_location(self, names):
        self.lstrip_text()
        pos = self.cur_pos()
        self.expect("[")
        process = self.parse_process(names)
        self.expect("]")

        return LocationToken(process, pos, self.cur_pos())


    def parse_location_list(self, names):
        self.lstrip_text()
        pos = self.cur_pos()

        locations = [self.parse_location(names)]

        self.lstrip_text()
        while self.wtext.startswith('|'):
            self.lstrip_pattern('|')
            locations.append(self.parse_location(names))
            self.lstrip_text()

        return LocationListToken(locations, pos, self.cur_pos())

class ParseException(Exception):
    def __init__(self, msg, pos):
        self.msg = msg
        self.pos = pos
        
class ParseDeclarationException(Exception):
    def __init__(self, msg, start, end):
        self.msg = msg
        self.start = start
        self.end = end