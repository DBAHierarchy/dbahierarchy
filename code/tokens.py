class Token():
    pass

class ParamToken(Token):
    def __init__(self, name, value, start = 0, end = 0):
        self.name = name
        self.value = value
        self.start = start
        self.end = end
        
    def __str__(self):
        return 'param '+str(self.name)+' = '+str(self.value)+'.\n'
        

class FunToken(Token):
    def __init__(self, ident, arity, private, start = 0, end = 0):
        self.ident = ident
        self.arity = int(arity)
        self.private = private
        self.start = start
        self.end = end
        
    def __str__(self):
        if self.private:
            return 'private fun '+str(self.ident)+'/'+str(self.arity)+'.\n'
        else:
            return 'fun '+str(self.ident)+'/'+str(self.arity)+'.\n'
                    
class DataToken(Token):
    def __init__(self, ident, arity, start = 0, end = 0):
        self.ident = ident
        self.arity = int(arity)
        self.start = start
        self.end = end
        
    def __str__(self):
        return 'data '+str(self.ident)+'/'+str(self.arity)+'.\n'
        
class FreeToken(Token):
    def __init__(self, idents, private, start = 0, end = 0):
        self.idents = idents
        self.private = private
        self.start = start
        self.end = end

    def __str__(self):
        if self.private:
            string = 'private free '
        else:
            string = 'free '
        first = True
        for ident in self.idents:
            if not first:
                string += ', '
            first = False
            string += str(ident)
        string += '.\n'
        return string
    
class ReducSeqToken(Token):
    def __init__(self, reducs, private, start = 0, end = 0):
        self.reducs = reducs
        self.private = private
        self.start = start
        self.end = end

    def size(self):
        return len(self.reducs)
    
    def __str__(self):
        string = ''
        first = True
        for reduc in self.reducs:
            if not first:
                string += ', '
            first = False
            string += str(reduc)
        return string        
        
            
class ReducToken(Token):
    def __init__(self, ident, terms, result, private, start = 0, end = 0):
        self.ident = ident
        self.terms = terms
        self.result = result
        self.private = private
        self.start = start
        self.end = end
        self.arity = terms.size()

    def __str__(self):
        string = 'reduc ' + str(self.ident) + str(self.terms) + ' = ' + str(self.result) + '.\n'
        return string
                         
class IdentToken(Token):
    def __init__(self, name, start = 0, end = 0):
        self.name = name
        self.start = start
        self.end = end
        self.declaration = None
        self.refs = []
        
    def __str__(self):
        return self.name

class IdentSeqToken(Token):
    def __init__(self, idents, start = 0, end = 0):
        self.idents = idents
        self.start = start
        self.end = end
        
    def __str__(self):
        string = ''
        first = True
        for ident in self.idents:
            if not first:
                string += ', '
            first = False
            string += str(ident)
        return string        
                             
class TermToken(Token):
    pass

class TermApplyToken(TermToken):
    def __init__(self, ident, args, start = 0, end = 0):
        self.ident = ident
        self.args = args
        self.start = start
        self.end = end        
        
    def __str__(self):
        return str(self.ident) + str(self.args)

class TermSeqToken(TermToken):
    def __init__(self, terms, start = 0, end = 0):
        self.terms = terms
        self.start = start
        self.end = end
    
    def size(self):
        return len(self.terms)

    def __str__(self):
        string = '('
        first = True
        for term in self.terms:
            if not first:
                string += ', '
            first = False
            string += str(term)
        string += ')'
        return string

class TermIdentToken(TermToken):
    def __init__(self, ident, start = 0, end = 0):
        self.ident = ident
        self.start = start
        self.end = end
        
    def __str__(self):
        return str(self.ident)        

class TermChoiceToken(TermToken):
    def __init__(self, term1, term2, start = 0, end = 0):
        self.term1 = term1
        self.term2 = term2
        self.start = start
        self.end = end                               
        
    def __str__(self):
        return 'choice[' + str(self.term1) + ', ' + str(self.term2) + ']'        
                             
class QuerySeqToken(Token):
    def __init__(self, queries, start = 0, end = 0):
        self.queries = queries
        self.start = start
        self.end = end
                                             
    def __str__(self):
        string = 'query '
        first = True
        for query in self.queries:
            if not first:
                string += '; '
            first = False
            string += str(query)
        string += '.\n'
        return string
                                             
class QueryToken(Token):
    pass

class QueryPutbeginEvInjToken(QueryToken):
    def __init__(self, idents, start = 0, end = 0):
        self.idents = idents
        self.start = start
        self.end = end
        
    def __str__(self):
        return 'putbegin evinj:' + str(self.idents)

class QueryPutbeginEvToken(QueryToken):
    def __init__(self, idents, start = 0, end = 0):
        self.idents = idents
        self.start = start
        self.end = end    
        
    def __str__(self):
        return 'putbegin ev:' + str(self.idents)

class QueryLetToken(QueryToken):
    def __init__(self, ident, term, start = 0, end = 0):
        self.ident = ident
        self.term = term
        self.start = start
        self.end = end

    def __str__(self):
        return 'let ' + str(self.ident) + ' = ' + str(self.term)

class QueryGFactToken(QueryToken):
    def __init__(self, fact, start = 0, end = 0):
        self.fact = fact
        self.start = start
        self.end = end

    def __str__(self):
        return str(self.fact)

class QueryRealToken(QueryToken):
    def __init__(self, fact, hyp, start = 0, end = 0):
        self.fact = fact
        self.hyp = hyp
        self.start = start
        self.end = end

    def __str__(self):
        return str(self.fact) + ' ==> ' + str(self.hyp)    
    
class GTermToken(Token):
    pass

class GTermApplyToken(GTermToken):
    def __init__(self, ident, args, start = 0, end = 0):
        self.ident = ident
        self.args = args
        self.start = start
        self.end = end
        
    def __str__(self):
	args = ''
	if self.args != None:
		args = str(self.args)

        return str(self.ident) + '(' + args + ')'        

class GTermBindToken(GTermToken):
    def __init__(self, ident, args, start = 0, end = 0):
        self.ident = ident
        self.args = args
        self.start = start
        self.end = end

    def __str__(self):
        return str(self.ident) + '[' + self.args + ']'
            
class GTermIdentToken(GTermToken):
    def __init__(self, ident, start = 0, end = 0):
        self.ident = ident
        self.start = start
        self.end = end 

    def __str__(self):
        return str(self.ident)                                   

class GTermSeqToken(GTermToken):
    def __init__(self, terms, start = 0, end = 0):
        self.terms = terms
        self.start = start
        self.end = end
    
    def size(self):
        return len(self.terms)

    def __str__(self):
        string = ''
        first = True
        for term in self.terms:
            if not first:
                string += ', '
            first = False
            string += str(term)
        return string

class GBindingSeqToken(Token):
    def __init__(self, terms, start = 0, end = 0):
        self.terms = terms
        self.start = start
        self.end = end
    
    def size(self):
        return len(self.terms)

    def __str__(self):
        string = ''
        first = True
        for query in self.queries:
            if not first:
                string += ', '
            first = False
            string += str(query)
        return string


class GBindingToken(Token):
    pass

class GBindingNToken(GBindingToken):
    def __init__(self, n, term, start = 0, end = 0):
        self.n = n
        self.term = term
        self.start = start
        self.end = end                            
        
    def __str__(self):
        return '!' + str(self.n) + ' = ' + self.term        

class GBindingIdentToken(GBindingToken):
    def __init__(self, ident, term, start = 0, end = 0):
        self.ident = ident
        self.term = term
        self.start = start
        self.end = end

    def __str__(self):
        return str(self.ident) + ' = ' + self.term        

                    
class GFactToken(Token):
    pass

class GFactIdentToken(GFactToken):
    def __init__(self, ident, terms, start = 0, end = 0):
        self.ident = ident
        self.terms = terms
        self.start = start
        self.end = end
        
    def __str__(self):
        if self.terms == None:
            return self.ident
        else:        
            return str(self.ident) + ':' + str(self.terms)
        

class GFactIdentPhaseToken(GFactToken):
    def __init__(self, ident, terms, phase, start = 0, end = 0):
        self.ident = ident
        self.terms = terms
        self.phase = phase
        self.start = start
        self.end = end

    def __str__(self):
        return str(self.ident) + ':' + str(self.terms) + ' phase ' + str(self.phase)

class GFactEqualToken(GFactToken):
    def __init__(self, term1, term2, start = 0, end = 0):
        self.term1 = term1
        self.term2 = term2
        self.start = start
        self.end = end

    def __str__(self):
        return str(self.term1) + ' = ' + str(self.term2)
    
class GFactUnequalToken(GFactToken):
    def __init__(self, term1, term2, start = 0, end = 0):
        self.term1 = term1
        self.term2 = term2
        self.start = start
        self.end = end 

    def __str__(self):
        return str(self.term1) + ' <> ' + str(self.term2)

        
class HypToken(Token):
    pass

class HypOrToken(HypToken):
    def __init__(self, hyp1, hyp2, start = 0, end = 0):                         
        self.hyp1 = hyp1
        self.hyp2 = hyp2
        self.start = start
        self.end = end 

    def __str__(self):
        return str(self.hyp1) + ' | ' + str(self.hyp2)

class HypAndToken(HypToken):
    def __init__(self, hyp1, hyp2, start = 0, end = 0):                         
        self.hyp1 = hyp1
        self.hyp2 = hyp2
        self.start = start
        self.end = end

    def __str__(self):
        return str(self.hyp1) + ' & ' + str(self.hyp2)
            
class HypGFactToken(HypToken):
    def __init__(self, fact, start = 0, end = 0):                         
        self.fact = fact
        self.start = start
        self.end = end

    def __str__(self):
        return str(self.fact)
    
class HypBracketToken(HypToken):
    def __init__(self, hyp, start = 0, end = 0):                         
        self.hyp = hyp
        self.start = start
        self.end = end

    def __str__(self):
        return '(' + str(self.hyp) + ')'
        
class HypRealQueryToken(HypToken):
    def __init__(self, query, start = 0, end = 0):                         
        self.query = query
        self.start = start
        self.end = end                        

    def __str__(self):
        return '(' + str(self.query) + ')'
    
class LetToken(Token):
    def __init__(self, ident, process, start = 0, end = 0):                         
        self.ident = ident
        self.process = process
        self.start = start
        self.end = end
        
    def __str__(self):
        return 'let ' + str(self.ident) + ' =\n' + str(self.process)  + '.\n'
        
class ProcessToken(Token):
    def __init__(self, process, start = 0, end = 0):                         
        self.process = process
        self.start = start
        self.end = end

    def __str__(self):
        return '\nprocess\n' + str(self.process)


class ProcessBracketsToken(ProcessToken):
    def __init__(self, process, start = 0, end = 0):                         
        self.process = process
        self.start = start
        self.end = end

    def __str__(self):
        return '(' + str(self.process) + ')'
        
class ProcessIdentToken(ProcessToken):
    def __init__(self, ident, start = 0, end = 0):                         
        self.ident = ident
        self.start = start
        self.end = end

    def __str__(self):
        return str(self.ident)
            
class ProcessBankToken(ProcessToken):
    def __init__(self, process, start = 0, end = 0):                         
        self.process = process
        self.start = start
        self.end = end

    def __str__(self):
        return '!(' + str(self.process) + ')'
        
class ProcessNullToken(ProcessToken):
    def __init__(self, start = 0, end = 0):
        self.start = start
        self.end = end

    def __str__(self):
        return '0'
    
class ProcessNewToken(ProcessToken):
    def __init__(self, ident, process, start = 0, end = 0):
        self.ident = ident                         
        self.process = process
        self.start = start
        self.end = end

    def __str__(self):
        out = 'new ' + str(self.ident)

        if self.process != None:
            out += ';\n' + str(self.process)

        return out

class ProcessIfToken(ProcessToken):
    def __init__(self, fact, processif, processelse, start = 0, end = 0):
        self.fact = fact                         
        self.processif = processif
        self.processelse = processelse
        self.start = start
        self.end = end
        
    def __str__(self):
        string = 'if ' + str(self.fact) + ' then\n' + str(self.processif);
        if not self.processelse == None:
            string += 'else\n'  + str(self.processelse)
        return string        
        
class ProcessInToken(ProcessToken):
    def __init__(self, channel, pattern, process, start = 0, end = 0):
        self.channel = channel                         
        self.pattern = pattern
        self.process = process
        self.start = start
        self.end = end            

    def __str__(self):
        string = 'in(' + str(self.channel) + ', ' + str(self.pattern) + ')';
        if not self.process == None:
            string += ';\n'  + str(self.process)
        return string
            
class ProcessOutToken(ProcessToken):
    def __init__(self, channel, term, process, start = 0, end = 0):
        self.channel = channel                         
        self.term = term
        self.process = process
        self.start = start
        self.end = end

    def __str__(self):
        string = 'out(' + str(self.channel) + ', ' + str(self.term) + ')';
        if not self.process == None:
            string += ';\n'  + str(self.process)
        return string        
        
class ProcessLetToken(ProcessToken):
    def __init__(self, pattern, term, process, processelse, start = 0, end = 0):    
        self.pattern = pattern                         
        self.term = term
        self.process = process
        self.processelse = processelse
        self.start = start
        self.end = end
        
    def __str__(self):
        string = 'let ' + str(self.pattern) + ' = ' + str(self.term) +' in\n' + str(self.process);
        if not self.processelse == None:
            string += 'else '  + str(self.processelse)
        return string        

class ProcessLetSuchthatToken(ProcessToken):
    def __init__(self, idents, fact, process, processelse, start = 0, end = 0):    
        self.idents = idents                         
        self.fact = fact
        self.process = process
        self.processelse = processelse
        self.start = start
        self.end = end

    def __str__(self):
        string = 'let ' + str(self.idents) + ' suchthat ' + str(self.fact) +' in\n' + str(self.process);
        if not self.processelse == None:
            string += 'else '  + str(self.processelse)
        return string
        
class ProcessParallelToken(ProcessToken):
    def __init__(self, process1, process2, start = 0, end = 0):
        self.process1 = process1
        self.process2 = process2
        self.start = start
        self.end = end
        
    def __str__(self):
        return str(self.process1) + ' | ' + str(self.process2)
            
class ProcessEventToken(ProcessToken):
    def __init__(self, term, process, start = 0, end = 0):    
        self.term = term
        self.process = process
        self.start = start
        self.end = end

    def __str__(self):
        string = 'event ' + str(self.term)
        if not self.process == None:
            string += ';\n'  + str(self.process)
        return string           
        
class ProcessPhaseToken(ProcessToken):
    def __init__(self, n, process, start = 0, end = 0):    
        self.n = n
        self.process = process
        self.start = start
        self.end = end

    def __str__(self):
        string = 'phase ' + str(self.n)
        if not self.process == None:
            string += ';\n'  + str(self.process)
        return string
            
class FactToken(Token):
    pass

class FactIdentToken(FactToken):
    def __init__(self, ident, terms, start = 0, end = 0):
        self.ident = ident
        self.terms = terms
        self.start = start
        self.end = end
        
    def __str__(self):
        return str(self.ident) + ':' + str(self.terms)    

class FactEqualToken(FactToken):
    def __init__(self, term1, term2, start = 0, end = 0):
        self.term1 = term1
        self.term2 = term2
        self.start = start
        self.end = end
        
    def __str__(self):
        return str(self.term1) + ' = ' + str(self.term2)        
            
class FactUnequalToken(FactToken):
    def __init__(self, term1, term2, start = 0, end = 0):
        self.term1 = term1
        self.term2 = term2
        self.start = start
        self.end = end

    def __str__(self):
        return str(self.term1) + ' <> ' + str(self.term2)
            
class PatternToken(Token):
    pass

class PatternBracketsToken(Token):
    def __init__(self, seq, start = 0, end = 0):
        self.seq = seq
        self.start = start
        self.end = end            
        
    def __str__(self):
        return '(' + str(self.seq) + ')'
        
class PatternApplyToken(Token):
    def __init__(self, ident, seq, start = 0, end = 0):
        self.ident = ident
        self.seq = seq
        self.start = start
        self.end = end        

    def __str__(self):
        return str(self.ident) + str(self.seq)
            
class PatternIdentToken(Token):
    def __init__(self, ident, start = 0, end = 0):
        self.ident = ident
        self.start = start
        self.end = end
        
    def __str__(self):
        return str(self.ident)                             
        
class PatternEqualToken(Token):
    def __init__(self, term, start = 0, end = 0):
        self.term = term
        self.start = start
        self.end = end

    def __str__(self):
        return '=' + str(self.term)
            
class PatternSeqToken(Token):
    def __init__(self, patterns, start = 0, end = 0):
        self.patterns = patterns
        self.start = start
        self.end = end

    def __str__(self):
        string = ''
        first = True
        for pattern in self.patterns:
            if not first:
                string += ', '
            first = False
            string += str(pattern)
        return string
    
class EquationToken(Token):
    def __init__(self, term1, term2, start = 0, end = 0):
        self.term1 = term1
        self.term2 = term2
        self.start = start
        self.end = end

    def __str__(self):
        return 'equation ' + self.term1 + ' = ' + self.term2 + '.\n'
    
class PredToken(Token):
    def __init__(self, ident, n, idents, start = 0, end = 0):
        self.ident = ident
        self.n = n
        self.idents = idents
        self.start = start
        self.end = end

    def __str__(self):
        return 'pred ' + self.ident + '/' + str(self.n) + ' ' + self.idents + '.\n'
    
class WeaksecretToken(Token):
    def __init__(self, ident, start = 0, end = 0):
        self.ident = ident
        self.start = start
        self.end = end

    def __str__(self):
        return 'weaksecret ' + self.ident + '.\n'
    
class InterfspecSeqToken(Token):
    def __init__(self, interfspecs, start = 0, end = 0):
        self.interfspecs = interfspecs
        self.start = start
        self.end = end

    def __str__(self):
        string = ''
        first = True
        for interfspec in self.interfspecs:
            if not first:
                string += ', '
            first = False
            string += str(interfspec)
        return string
    
class InterfspecToken(Token):
    def __init__(self, ident, terms, start = 0, end = 0):
        self.ident = ident
        self.terms = terms
        self.start = start
        self.end = end
        
    def __str__(self):
        string = self.ident
        if not self.terms == None:
            string += 'among (' + self.terms + ')'
        return string
    
class NoninterfToken(Token):
    def __init__(self, interfspecs, start = 0, end = 0):
        self.interfspecs = interfspecs
        self.start = start
        self.end = end
                               
    def __str__(self):
        return 'noninterf ' + self.interfspecs + '.\n'      
    
class NounifToken(Token):
    def __init__(self, gfactformat, n, b, start = 0, end = 0):
        self.gfactformat = gfactformat
        self.n = n
        self.b = b
        self.start = start
        self.end = end
                               
    def __str__(self):
        string =  'nounif ' + self.factformat
        if not self.n == None:
            string += '/' + self.n
            
        start = True
        if len(self.b) > 0:
            for b in self.b:
                start = False
                if not start:
                    string += ';'
                string += ' ' + b
            
        return string + '.\n'
    
class FactformatToken(Token):
    def __init__(self, ident, termformats, start = 0, end = 0):
        self.ident = ident
        self.termformats = termformats
        self.start = start
        self.end = end   
        
    def __str__(self):
        return self.ident + ':' + self.termformats
    
class TermformatToken(Token):
    pass

class TermformatSeqToken(TermformatToken):
    def __init__(self, termformats, start = 0, end = 0):
        self.termformats = termformats
        self.start = start
        self.end = end
        
    def __str__(self):
        string = ''
        first = True
        for termformat in self.termformats:
            if not first:
                string += ', '
            first = False
            string += str(termformat)
        return string        

class TermformatStarToken(TermformatToken):
    def __init__(self, ident, start = 0, end = 0):
        self.ident = ident
        self.start = start
        self.end = end
        
    def __str__(self):
        return '*' + self.ident
    
class TermformatApplyToken(TermformatToken):
    def __init__(self, ident, termformats, start = 0, end = 0):
        self.ident = ident
        self.termformats = termformats
        self.start = start
        self.end = end
        
    def __str__(self):
        return self.ident + '(' + self.termformats + ')'
    
class TermformatIdentToken(TermformatToken):
    def __init__(self, ident, start = 0, end = 0):
        self.ident = ident
        self.start = start
        self.end = end
        
    def __str__(self):
        return self.ident
    
class BindToken(Token):
    def __init__(self, ident, gtermformat, start ,end):
        self.ident = ident
        self.gtermformat = gtermformat
        self.start = start
        self.end = end
        
    def __str__(self):
        return self.ident + ' = ' + self.gtermformat
    
class GFactformatToken(Token):
    def __init__(self, ident, gtermformats, n, start = 0, end = 0):
        self.ident = ident
        self.gtermformats = gtermformats
        self.n = n
        self.start = start
        self.end = end
        
    def __str__(self):
        string = self.ident + ':' + self.gtermformats
        if not self.n == None:
            string += ' phase ' + self.n
        return string              
    
class GTermformatToken(Token):
    pass

class GTermformatSeqToken(GTermformatToken):
    def __init__(self, gtermformats, start = 0, end = 0):
        self.gtermformats = gtermformats
        self.start = start
        self.end = end
        
    def __str__(self):
        string = ''
        first = True
        for gtermformat in self.gtermformats:
            if not first:
                string += ', '
            first = False
            string += str(gtermformat)
        return string        

class GTermformatStarToken(GTermformatToken):
    def __init__(self, ident, start = 0, end = 0):
        self.ident = ident
        self.start = start
        self.end = end
        
    def __str__(self):
        return '*' + self.ident
    
class GTermformatApplyToken(GTermformatToken):
    def __init__(self, ident, gterms, start = 0, end = 0):
        self.ident = ident
        self.gterms = gterms
        self.start = start
        self.end = end
        
    def __str__(self):
        return self.ident + '(' + self.gtermformats + ')'

class GTermformatBindToken(GTermformatToken):
    def __init__(self, ident, gbindings, start = 0, end = 0):
        self.ident = ident
        self.gbindings = gbindings
        self.start = start
        self.end = end
        
    def __str__(self):
        return self.ident + '[' + self.gbindings + ']'
        
class GTermformatIdentToken(GTermformatToken):
    def __init__(self, ident, start = 0, end = 0):
        self.ident = ident
        self.start = start
        self.end = end
        
    def __str__(self):
        return self.ident
    
class ElimtrueToken(Token):
    def __init__(self, factformat, start = 0, end = 0):
        self.factformat = factformat
        
    def __str__(self):
        return 'elimtrue' + self.factformat + '\n'
    
class NotToken(Token):
    def __init__(self, gfact, b, start = 0, end = 0):
        self.gfact = gfact
        self.b = b
        self.start = start
        self.end = end                   

    def __str__(self):
        string =  'not ' + self.gfact
            
        start = True
        if len(self.b) > 0:
            for b in self.b:
                start = False
                if not start:
                    string += ';'
                string += ' ' + b
            
        return string + '.\n'        
    
class ClausesToken(Token):
    def __init__(self, rules, start = 0, end = 0):
        self.rules = rules
        self.start = start
        self.end = end
        
    def __str__(self):
        string =  'clauses '
            
        start = True
        for rule in self.rules:
            start = False
            if not start:
                string += ';'
            string += ' ' + rule
            
        return string + '.\n'           
    
class RuleToken(Token):
    pass

class RuleFactToken(RuleToken):
    def __init__(self, fact, start = 0, end = 0):
        self.fact = fact
        self.start = start
        self.end = end
        
    def __str__(self):
        return self.fact
    
class RuleImpToken(RuleToken):
    def __init__(self, facts, fact, start = 0, end = 0):
        self.facts = facts
        self.fact = fact
        self.start = start
        self.end = end
        
    def __str__(self):
        string =  ''
            
        start = True
        for fact in self.facts:
            start = False
            if not start:
                string += ' & '
            string += fact
            
        return string + ' -> ' + self.fact + '.\n'
    
class RuleBiImpToken(RuleToken):
    def __init__(self, facts, fact, start = 0, end = 0):
        self.facts = facts
        self.fact = fact
        self.start = start
        self.end = end
        
    def __str__(self):
        string =  ''
            
        start = True
        for fact in self.facts:
            start = False
            if not start:
                string += ' & '
            string += fact
            
        return string + ' <-> ' + self.fact + '.\n'
    
class RuleDoubleBiImpToken(RuleToken):
    def __init__(self, facts, fact, start = 0, end = 0):
        self.facts = facts
        self.fact = fact
        self.start = start
        self.end = end
        
    def __str__(self):
        string =  ''
            
        start = True
        for fact in self.facts:
            start = False
            if not start:
                string += ' & '
            string += fact
            
        return string + ' <=> ' + self.fact + '.\n'    

class ProcessStartTimerToken(ProcessToken):
    def __init__(self, process, start = 0, end = 0):    
        self.process = process
        self.start = start
        self.end = end

    def __str__(self):
        string = 'startTimer'
        if not self.process == None:
            string += ';\n'  + str(self.process)
        return string


class ProcessStopTimerToken(ProcessToken):
    def __init__(self, process, start = 0, end = 0):    
        self.process = process
        self.start = start
        self.end = end

    def __str__(self):
        string = 'stopTimer'
        if not self.process == None:
            string += ';\n'  + str(self.process)
        return string

class LocationListToken(Token):
    def __init__(self, locations, start = 0, end = 0):
        self.locations = locations
        self.start = start
        self.end = end

    def __str__(self):
        return ' | '.join([str(loc) for loc in  self.locations])

class LocationToken(Token):
    def __init__(self, process, start = 0, end = 0):
        self.process = process
        self.start = start
        self.end = end

    def __str__(self):
        return '[' + str(self.process) + ']'